﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour {

	public GameObject LilBall;
	private bool BallActive;
    public float NumShots;

    // Use this for initialization
    void Start () 
	{
		GameObject.Find ("LilBall");
		BallActive = false;
        NumShots = 3;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (Input.GetKeyDown (KeyCode.Space) && BallActive == false) 
		{
			gameObject.GetComponent<Rigidbody> ().AddForce (LilBall.transform.localPosition * 200, ForceMode.Impulse);
            NumShots = NumShots - 1;
            Debug.Log(NumShots);
            GameObject.Find("TurnsLeft").GetComponent<Text>().text = NumShots.ToString();
            
		}

        if (NumShots == 0)
        {
            BallActive = true;
        }
	}
}
