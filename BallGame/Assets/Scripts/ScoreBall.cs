﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBall : MonoBehaviour {

    public float MainScoreNum;

	// Use this for initialization
	void Start ()
    {
        MainScoreNum = 0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnColliderEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            MainScoreNum = MainScoreNum + 100f;
            GameObject.Find("MainScore").GetComponent<Text>().text = MainScoreNum.ToString();
        }
    }
}
