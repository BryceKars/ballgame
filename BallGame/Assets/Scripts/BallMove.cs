﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {

	public GameObject Ball;
    public PlayerMove PlayerMove;
	private bool MarkerActive; 
	public float SpinSpeed;

	// Use this for initialization
	void Start () 
	{
		MarkerActive = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (MarkerActive == true) 
		{
			transform.RotateAround(Ball.transform.position, Vector3.forward, SpinSpeed * Time.deltaTime);
		}

		if (Input.GetKeyDown (KeyCode.Space) && PlayerMove.NumShots <= 1) 
		{
			MarkerActive = false;
			gameObject.SetActive (false);
		}
	}
}
